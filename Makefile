POETRY=poetry

# Install python package dependencies
.PHONY: install
install:
	${POETRY} env use python3.9
	${POETRY} install

# Update python package dependencies
.PHONY: update
update:
	${POETRY} update

# Starts the development server
.PHONY: run
run:
	${POETRY} run uvicorn demo_api.app:app

# Lint using black
.PHONY: format
format:
	${POETRY} run autoflake --remove-all-unused-imports --recursive --remove-unused-variables --in-place demo_api tests
	${POETRY} run black --line-length=80 demo_api tests
	${POETRY} run isort demo_api tests

# Run tests
.PHONY: test
test:
	${POETRY} run pytest tests