import logging

from fastapi import FastAPI
from fastapi.responses import JSONResponse

from demo_api.models.httpErrors import HTTPErrorModel
from demo_api.models.httpParameters import CryptoAmount, FiatPrice
from demo_api.utils.bitstamp import Bitstamp
from demo_api.utils.calculator import Calculator, OrderBookError
from demo_api.utils.string import DecimalUtils

logger = logging.getLogger("uvicorn")

app = FastAPI(title="Bitstack Demo API")
app.bitstamp = Bitstamp()
app.bitstamp.startAskOrderBookUpdateJob()
while app.bitstamp.getLastAskOrderBookUpdateDateTime() == None:
    pass


@app.on_event("shutdown")
def shutdown_event():
    app.bitstamp.stopAskOrderBookUpdateJob()


@app.get("/")
def root_get():
    return {"description": "Hello coder!"}


@app.post(
    "/purchase_offer",
    response_model=FiatPrice,
    responses={409: {"model": HTTPErrorModel}},
)
def purchase_offer(body: CryptoAmount):
    """
    Post methode expecting to get a BTC amount
    and return an cost EUR price from Bitstamp ask order book
    """
    logger.info("Get asks order book from Bitstamp...")

    asks = app.bitstamp.getAskOrderBook()

    logger.info(
        "".join(
            [
                "Start price calculation for ",
                DecimalUtils.toStringFormat(body.amount),
                " BTC...",
            ]
        )
    )

    try:
        price = round(Calculator.priceRecursive(asks, body.amount), 2)

    except (RecursionError, OrderBookError) as err:
        logger.error(err.message)
        return JSONResponse(status_code=409, content={"message": err.message})

    logger.info(
        "".join(
            [
                "Total price: ",
                DecimalUtils.toStringFormat(price),
                " EUR for ",
                DecimalUtils.toStringFormat(body.amount),
                " BTC bought",
            ]
        )
    )

    logger.info(
        "".join(
            [
                "Average price: ",
                DecimalUtils.toStringFormat(round(price / body.amount, 2)),
                " EUR/BTC",
            ]
        )
    )

    return {"price": price}
