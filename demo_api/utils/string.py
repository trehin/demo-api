from decimal import Decimal


class DecimalUtils:
    @staticmethod
    def toStringFormat(decimal: Decimal):
        return "{:f}".format(decimal)
