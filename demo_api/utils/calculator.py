import logging
from decimal import Decimal

from demo_api.utils.string import DecimalUtils

logger = logging.getLogger("uvicorn")


class AmountError(Exception):
    def __init__(
        self,
        message="Do not make sense to calculate a price from an empty or negative amount",
    ):
        self.message = message
        super().__init__(self.message)


class OrderBookError(Exception):
    def __init__(
        self,
        message="Order book does not contain enough amount to satisfy the demand",
    ):
        self.message = message
        super().__init__(self.message)


class Calculator:
    """
    Calculate cost price for a specific amount in recursive from an orderbook
    """

    @staticmethod
    def priceRecursive(
        asks: list,
        cryptoAmount: Decimal,
        fiatPrice: Decimal = Decimal(0),
        index: int = 0,
    ):
        if cryptoAmount <= 0 or cryptoAmount is None:
            raise AmountError()

        if index == len(asks):
            raise OrderBookError()

        if cryptoAmount <= Decimal(asks[index][1]):
            fiatPrice = round(
                fiatPrice + cryptoAmount * Decimal(asks[index][0]), 2
            )
            logger.debug(
                DecimalUtils.toStringFormat(cryptoAmount)
                + " BTC for "
                + DecimalUtils.toStringFormat(fiatPrice)
                + " EUR at "
                + asks[index][0]
                + " EUR/BTC (Remaining amount: 0 BTC)"
            )

            logger.info(
                "Price calculation ended with " + str(index + 1) + " order(s)."
            )
            logger.info(
                "Ask order book: "
                + "".join(str(i) for i in asks[0 : index + 1])
            )

            return fiatPrice

        else:
            fiatPrice = round(
                fiatPrice + Decimal(asks[index][1]) * Decimal(asks[index][0]), 2
            )
            cryptoAmount -= Decimal(asks[index][1])
            logger.debug(
                asks[index][1]
                + " BTC for "
                + DecimalUtils.toStringFormat(fiatPrice)
                + " EUR at "
                + asks[index][0]
                + " EUR/BTC (Remaining amount: "
                + DecimalUtils.toStringFormat(cryptoAmount)
                + " BTC)"
            )

            index += 1

            try:
                return Calculator.priceRecursive(
                    asks, cryptoAmount, fiatPrice, index
                )

            except RecursionError as err:
                err.message = "Unable to calculate for a large amount"
                raise err
