import json
import logging
import threading
import copy
import time
from datetime import datetime
from threading import Thread, Lock
from urllib.request import Request

import requests

from demo_api.utils.singleton import Singleton

logger = logging.getLogger("uvicorn")


class Bitstamp(metaclass=Singleton):
    """
    Pull ask order book data in memory from a thread
    calling Bitstamp API to request ask order book
    every 10 secondes
    """

    BASE_URL: str = "https://www.bitstamp.net/api/v2/"
    ORDER_BOOK_METHODE: str = "order_book/BTCEUR/"

    _thread: Thread = None
    _threadLock = Lock()

    _askOrderBook: list = None
    _askOrderBookUpdateTime: int = 10
    _lastAskOrderBookUpdateDateTime: datetime = None

    def __init__(self):
        self._thread = threading.Thread(target=self._updateAskOrderBook)

    def startAskOrderBookUpdateJob(self):
        self._runThread = True
        self._thread.start()

    def stopAskOrderBookUpdateJob(self):
        logger.info("Stop ask order book update job...")
        self._runThread = False

        while True:
            if self._thread.is_alive() == False:
                break

    def getAskOrderBook(self):
        with self._threadLock:
            return copy.copy(self._askOrderBook)

    def getLastAskOrderBookUpdateDateTime(self):
        with self._threadLock:
            return copy.copy(self._lastAskOrderBookUpdateDateTime)

    def _updateAskOrderBook(self):
        logger.info(
            "".join(
                [
                    "Start ask order book update Job exectued every ",
                    str(self._askOrderBookUpdateTime),
                    " second(s)...",
                ]
            )
        )
        url: str = "".join([self.BASE_URL, self.ORDER_BOOK_METHODE])

        while self._runThread:
            logger.info("Update ask order book in memory...")

            response: Request = None
            while response == None or response.status_code != 200:
                logger.debug("".join(["call ", url, " ..."]))
                response = requests.get(url)
                logger.debug(
                    "".join(
                        ["Response status code: ", str(response.status_code)]
                    )
                )
            with self._threadLock:
                self._askOrderBook = json.loads(response.text)["asks"]
                self._lastAskOrderBookUpdateDateTime = datetime.now()

            for _ in range(int(self._askOrderBookUpdateTime / 0.5)):
                if self._runThread == False:
                    break
                time.sleep(0.5)
