from decimal import Decimal

from pydantic import BaseModel, Extra, Field


class CryptoAmount(BaseModel, extra=Extra.forbid):
    amount: Decimal = Field(title="BTC amount", decimal_places=8, gt=0)


class FiatPrice(BaseModel):
    price: Decimal = Field(title="EUR price", decimal_places=2, gt=0)
