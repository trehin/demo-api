from pydantic import BaseModel


class HTTPErrorModel(BaseModel):
    message: str
