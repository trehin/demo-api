FROM python:3.9-slim

RUN apt-get update && \
    apt-get install --no-install-recommends -y curl

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PYTHONDONTWRITEBYTECODE=1 \
    # poetry:
    POETRY_VERSION=1.1.13 \
    POETRY_NO_INTERACTION=1 \
    POETRY_CACHE_DIR='/var/cache/pypoetry' \
    PATH="$PATH:/root/.local/bin"

RUN curl -sSL https://install.python-poetry.org | python -

WORKDIR /app
COPY . .

RUN poetry install --no-dev

ENV FASTAPI_ENV=production
EXPOSE 8000

CMD ["poetry", "run", "uvicorn", "--host", "0.0.0.0", "demo_api.app:app"]