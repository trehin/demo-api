from datetime import timedelta
from decimal import Decimal

import hypothesis
import requests
import schemathesis
from starlette.testclient import TestClient

from demo_api.app import app
from demo_api.utils.calculator import AmountError, Calculator, OrderBookError

schemathesis.fixups.install(["fast_api"])
schema = schemathesis.from_asgi("/openapi.json", app)


class TestFuzz:
    @schema.parametrize()
    # @settings(max_examples=1000)
    @hypothesis.settings(
        suppress_health_check=[
            hypothesis.HealthCheck.filter_too_much,
            hypothesis.HealthCheck.too_slow,
        ]
    )
    def test_fuzz(self, case):
        client = TestClient(case.app)
        response: requests.Response = case.call(
            session=client,
        )
        assert response.elapsed < timedelta(milliseconds=100)
        case.validate_response(response)
        case.app.bitstamp.stopAskOrderBookUpdateJob()

    def test_purchase_offer(self):
        client = TestClient(app)

        response = client.post("/purchase_offer", json={"amount": 0.12345678})
        assert response.status_code == 200

        response = client.post("/purchase_offer", json={"amount": 0.123456789})
        assert response.status_code == 422

        response = client.post("/purchase_offer", json={"amount": 99999999999})
        assert response.status_code == 409
        app.bitstamp.stopAskOrderBookUpdateJob()

    def test_priceRecursive(self):
        try:
            price = Calculator.priceRecursive([["1000", "1"]], Decimal("0"))
        except Exception as err:
            assert isinstance(err, AmountError)

        try:
            price = Calculator.priceRecursive(
                [["1000", "1"], ["1001", "2"], ["1003", "3"]], Decimal("-10")
            )
        except Exception as err:
            assert isinstance(err, AmountError)

        try:
            price = Calculator.priceRecursive(
                [
                    ["1000.42", "0.12345678"],
                    ["1000.99", "0.12345678"],
                    ["1000.76", "0.12345678"],
                ],
                Decimal("0.37037035"),
            )
        except Exception as err:
            assert isinstance(err, OrderBookError)

        price = Calculator.priceRecursive(
            [
                ["1000.42", "0.12345678"],
                ["1000.99", "0.12345678"],
                ["1000.76", "0.12345678"],
            ],
            Decimal("0.37037034"),
        )
        assert str(price) == "370.64"
